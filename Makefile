
.PHONY: build compile clean haml sass coffee resources deploy

MINIFY := juicer -q merge -s

build:
	@$(MAKE) clean
	@echo "Copying resources..."
	@$(MAKE) resources
	@echo "Compiling..."
	@$(MAKE) compile
	@echo "=> Site built!"

resources:
	@cp -rf resources/* output/

compile: haml sass coffee

haml:
	@haml haml/index.haml output/index.html
	@mkdir -p output/press
	@haml haml/press/index.haml output/press/index.html

sass:
	@sass sass/main.sass output/main.css
	@$(MINIFY) output/main.css
	@rm -f output/main.css

coffee:

clean:
	@mkdir -p output/
	@rm -rf output/*

deploy: build
	s3cmd -P sync --delete-removed output/* s3://thechoice-game.com/

